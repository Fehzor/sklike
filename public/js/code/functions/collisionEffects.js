/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//A has effect on B
var collisionEffects = {
    getEffect : function (num,args){
        switch (num){
            case 0:
                return collisionEffects.collide;
                break;
            case 1:
                return collisionEffects.knockback(args);
                break;
        }
        console.log("FALSE");
        return false;
    },
    collide : function(a,b){
        var aPos = a.getComponent("position").vector;
        var bPos = b.getComponent("position").vector;

        var angle = aPos.angleTo(bPos);
        var aVel = a.getComponent("velocity");
        
        if(!aVel){
            return false;
        } else {
            aVel=aVel.vector;
        }
        
        var ySub = aVel.length() * Math.sin(angle);
        var xSub = aVel.length() * Math.sin(angle);

        if(aPos.x > bPos.x){
            xSub = xSub * -1;
        }

        if(aPos.y > bPos.y){
            ySub = ySub * -1;
        }

        aVel.x -= xSub;
        aVel.y -= ySub;
    },
    knockback : function(howMuch){
        return function(a,b){
            var aPos = a.getComponent("position").vector;
            var bPos = b.getComponent("position").vector;

            var angle = aPos.angleTo(bPos);
            var aVel = a.getComponent("velocity");

            if(!aVel){
                return false;
            } else {
                aVel=aVel.vector;
            }

            var ySub = howMuch * Math.sin(angle);
            var xSub = howMuch * Math.sin(angle);

            if(aPos.x > bPos.x){
                xSub = xSub * -1;
            }

            if(aPos.y > bPos.y){
                ySub = ySub * -1;
            }

            aVel.x -= xSub;
            aVel.y -= ySub;
        }
    }
}
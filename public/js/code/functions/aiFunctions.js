/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Functions used by AIs on state machines to control them.
//Basically, a type of AI
//machine = the machine it acts on
//done = is the current action acting still or did it "finish"?
var aiFunctions = {
    //Linear AI does a state, then moves on. It will easily get stuck.
    //Would be useful for turrets- aim, turn, shoot, rinse, repeat.
    linearAI : function(machine, done){
        if(done){
            if(machine.current.options.length > 0){
                machine.current = superRandom.randomElement(machine.current.options);
                machine.steps = 0;
            }
        } else {
            
        }
    },
    //Doesn't necessarily wait for finish, and progresses randomly. Feels robotic.
    stepTimerAI : function(steps){
        return function(machine, done){
            if(done){
                if(machine.current.options.length > 0){
                    machine.current = superRandom.randomElement(machine.current.options);
                    machine.steps = 0;
                }
            } else {
                if(machine.steps > steps){
                    machine.current = superRandom.randomElement(machine.current.options);
                    machine.steps = 0;
                }
            }
        }
    },
    idleUntil : function(machine, done){
        if(done){
            if(machine.current.options.length > 0){
                for(var state in machine.current.options){
                    if(state.type == TYPE_IDLE){
                        machine.current = state;
                        machine.steps = 0;
                        return;
                    }
                }
                machine.current = superRandom.randomElement(machine.current.options);
                machine.steps = 0;
            }
        } else {
            var state = machine.current;
            if (state.type !== TYPE_IDLE){
                if(machine.steps > 300){
                    for(var state in machine.current.options){
                        if(state.type == TYPE_IDLE){
                            machine.current = state;
                            machine.steps = 0;
                            return;
                        }
                    }
                    machine.current = superRandom.randomElement(machine.current.options);
                    machine.steps = 0;
                }
            }
        }
    }
}

//Functions used by state machines to control enemies
//These should be or return a function that acts on an entity.
var stateFunctions = {
    test : function (message){
        return function(entity){
            console.log("The AI is going! Message: "+message);
            return true;
        }
    },
    //We may desire to make a list of players in the future, since there could be a large number of these.
    waitUntilClose : function(maxDist){
        return function(entity){
            var myPos = entity.getComponent("position");
            var pos = gameHandler.player.getComponent("position");
            var dist = pos.vector.distanceTo(myPos.vector);
            //console.log(dist+" < "+maxDist);;
            if(dist < maxDist){
                var vel = entity.getComponent("velocity");
                vel.vector.x = 0;
                vel.vector.y = 0;
                return true;
            }
            return false;
        }
    },
    moveTowardsVector : function(vector, stopDist){
        if(!stopDist){
            stopDist = .3;
        }
        return function(entity){
            var pos = entity.getComponent("position").vector;
            var vel = entity.getComponent("velocity");

            var distX = pos.x - vector.x;
            var distY = pos.y - vector.y;
            if(Math.abs(distX) < 1 && Math.abs(distY) < 1){
                vel.vector.x = 0;
                vel.vector.y = 0;
                //console.log("RETURNING FALSE");
                return true;
            } else {
                var distance = Math.sqrt(distX*distX + distY*distY);
                var xAdd = Math.abs(distX/distance) * vel.max;
                var yAdd = Math.abs(distY/distance) * vel.max;
                
                if(Math.abs(distX) > stopDist){
                    if(vector.x > pos.x){
                        vel.vector.x = xAdd;
                    } else {
                        vel.vector.x = xAdd * -1;
                    }
                } else {
                    vel.vector.x = 0;
                }
                if(Math.abs(distY) > stopDist){
                    if(vector.y > pos.y){
                        vel.vector.y = yAdd;
                    } else {
                        vel.vector.y = yAdd * -1;
                    }
                } else {
                    vel.vector.y = 0;
                }
                //console.log("RETURNING TRUE");
                return false;
            }
        }
    },
    moveTowardsEntity : function(moveTo,dist){
        var pos = moveTo.getComponent("position");
        return stateFunctions.moveTowardsVector(pos.vector,dist);
    },
    moveTowardsTarget : function(stopDist){
        if(!stopDist){
            stopDist = .3;
        }
        return function(entity){
            var targ = entity.getComponent("target");
            var vector = targ.target;
            if(!vector){
                return true;
            }
            
            var pos = entity.getComponent("position").vector;
            var vel = entity.getComponent("velocity");

            var distX = pos.x - vector.x;
            var distY = pos.y - vector.y;
            if(Math.abs(distX) < 1 && Math.abs(distY) < 1){
                vel.vector.x = 0;
                vel.vector.y = 0;
                //console.log("RETURNING FALSE");
                return true;
            } else {
                var distance = Math.sqrt(distX*distX + distY*distY);
                var xAdd = Math.abs(distX/distance) * vel.max;
                var yAdd = Math.abs(distY/distance) * vel.max;
                
                if(Math.abs(distX) > stopDist){
                    if(vector.x > pos.x){
                        vel.vector.x = xAdd;
                    } else {
                        vel.vector.x = xAdd * -1;
                    }
                } else {
                    vel.vector.x = 0;
                }
                if(Math.abs(distY) > stopDist){
                    if(vector.y > pos.y){
                        vel.vector.y = yAdd;
                    } else {
                        vel.vector.y = yAdd * -1;
                    }
                } else {
                    vel.vector.y = 0;
                }
                //console.log("RETURNING TRUE");
                return false;
            }
        }
    },
    strafeCounterClockwise : function(entity){
        var pos = entity.getComponent("position").vector;
        var vector = entity.getComponent("target").target;
        var vel = entity.getComponent("velocity");
        
        var machine = entity.getComponent("state");
        if(machine.steps > 35){
            vel.vector.x = 0;
            vel.vector.y = 0;
            return true;
        }

        var distX = pos.x - vector.x;
        var distY = pos.y - vector.y;
        
        var distance = Math.sqrt(distX*distX + distY*distY);
        var xAdd = Math.abs(distX/distance) * vel.max;
        var yAdd = Math.abs(distY/distance) * vel.max;

        if(Math.abs(distX) > 0){
            if(vector.x < pos.x){
                vel.vector.y = xAdd;
            } else {
                vel.vector.y = xAdd * -1;
            }
        } else {
            vel.vector.x = 0;
        }
        if(Math.abs(distY) > 0){
            if(vector.y > pos.y){
                vel.vector.x = yAdd;
            } else {
                vel.vector.x = yAdd * -1;
            }
        } else {
            vel.vector.y = 0;
        }
        //console.log("RETURNING TRUE");
        return false;

    },
    strafeClockwise : function(entity){
        var pos = entity.getComponent("position").vector;
        var vector = entity.getComponent("target").target;
        var vel = entity.getComponent("velocity");
        
        //Wait- check if it's been a while.
        var machine = entity.getComponent("state");
        if(machine.steps > 35){
            vel.vector.x = 0;
            vel.vector.y = 0;
            return true;
        }

        var distX = pos.x - vector.x;
        var distY = pos.y - vector.y;
        
        var distance = Math.sqrt(distX*distX + distY*distY);
        var xAdd = Math.abs(distX/distance) * vel.max;
        var yAdd = Math.abs(distY/distance) * vel.max;

        if(Math.abs(distX) > 0){
            if(vector.x > pos.x){
                vel.vector.y = xAdd;
            } else {
                vel.vector.y = xAdd * -1;
            }
        } else {
            vel.vector.x = 0;
        }
        if(Math.abs(distY) > 0){
            if(vector.y < pos.y){
                vel.vector.x = yAdd;
            } else {
                vel.vector.x = yAdd * -1;
            }
        } else {
            vel.vector.y = 0;
        }
        //console.log("RETURNING TRUE");
        return false;
    },
    moveTowards : function(x,y,dist){
        var pos = new THREE.Vector3(x,y,0);
        return stateFunctions.moveTowardsVector(pos,dist);
    },
    rise : function(height){
        return function(entity){
            var pos = entity.getComponent("position");
            var vel = entity.getComponent("velocity");
            if(pos.vector.z > height){
                return true;
            } else {
                pos.vector.z += .05;
                return false;
            }
        }
    },
    descend : function(height){
        return function(entity){
            var pos = entity.getComponent("position");
            var vel = entity.getComponent("velocity");
            if(pos.vector.z < height){
                return true;
            } else {
                pos.vector.z -= .05;
                return false;
            }
        }
    },
    //Takes 2 functions in, and does a then b, etc.
    //E.G. Rise then descend
    aORb : function(a,b){
        return function(entity){
            var machine = entity.getComponent("state");
            if(machine.stateInfo["a"]){
                if(a(entity)){
                    machine.stateInfo["a"] = false;
                    return true;
                }
            } else {
                if(b(entity)){
                    machine.stateInfo["a"] = true;
                    return true;
                }
            }
        }
    }
}
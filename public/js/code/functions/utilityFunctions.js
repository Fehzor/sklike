/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const NORTH = new THREE.Vector3(1, 0, 0);
const HALFPI = Math.PI / 2;
const tmp = new THREE.Vector3();

var angleFunctions = {
    faceVelocity : function(entity){
        const rot = entity.getComponent("rotation");
        const model = entity.getComponent("model");
        const vel = entity.getComponent("velocity");
        
        if(!vel.vector.x && !vel.vector.y) return;
        tmp.copy(model.model.position).add(vel.vector);
        model.model.lookAt(tmp);
        rot.angle = [model.model.rotation.x,
            model.model.rotation.y,
            model.model.rotation.z];
    },
    faceControls : function(entity){
        const rot = entity.getComponent("rotation");
        const model = entity.getComponent("model");
        const ctrl = entity.getComponent("controls");
        
        if(!ctrl.rx && !ctrl.ry) return;
        tmp.copy(model.model.position);
        tmp.x += ctrl.rx;
        tmp.y -= ctrl.ry;
        model.model.lookAt(tmp);
        rot.angle = [
            model.model.rotation.x,
            model.model.rotation.y,
            model.model.rotation.z,
        ];
    },
    faceTarget : function(entity){
        if(entity.hasComponent("target")){
            var targ = entity.getComponent("target");
            var pos = targ.target;
            if(pos){
                angleFunctions.facePoint(entity,pos);
            } else {
                angleFunctions.faceVelocity(entity);
            }
        } else {
            angleFunctions.faceVelocity(entity);
        }
    },
    applyRotation : function(entity){
        const rot = entity.getComponent("rotation");
        const model = entity.getComponent("model");
        model.model.rotation.x = rot.angle[0];
        model.model.rotation.y = rot.angle[1];
        model.model.rotation.z = rot.angle[2];
        //console.log("SETTING TO "+rot.angle);
    },
    
    //THESE ARE HELPER FUNCTIONS FOR THE ABOVE
    facePoint : function(entity, vect){
        const model = entity.getComponent("model");
        const rot = entity.getComponent("rotation");
        if(vect){
            model.model.lookAt(vect);
            rot.angle = [model.model.rotation.x,
            model.model.rotation.y,
            model.model.rotation.z];
        }
    }
}

//Functions for targetting something.
var targetFunctions = {
    //For now, we just target player 1 and don't worry about player 2,3, etc.
    selectPlayer : function(entity){
        var p1 = gameHandler.player;
        var playerPos = p1.getComponent("position").vector;
        var myPos = entity.getComponent("position").vector;
        
        var pos = playerPos.sub(myPos);
        //Basically, we want to use the calculated playerPos and then reset it.
        playerPos.add(myPos);
        
        return pos;
    },
    //Used to always return true- i.e. always lock on to this point.
    dedicated : function(entity){
        return true;
    }
}

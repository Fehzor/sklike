/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//These mostly just show off how we can use multiple damage formulas for
//calculating that sort of thing. So armor A can be percentage based,
//Armor B can be best for big attacks, and armor C can be best for small attacks.
var damageFormulas = {
    percentage : function(percentRemaining){
        return new function(damage){
            return Math.floor(damage * percentRemaining);
        }
    },
    bluntresist : function(damage){
        if(damage >= 100){
            damage = damage / 5;
        } else {
            damage = damage + 50;
        }
        
        return damage;
    },
    bulletproof : function(damage){
        if(damage < 100){
            damage = damage / 5;
        }  else {
            damage = damage + 50;
        }
        
        return damage;
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var triggers = {
    enterMap : function(mapID){
        return function(){
            for(var i = 0; i < gameHandler.entityList.length; ++i){
                var play = gameHandler.entityList[i];
                if(play.hasComponent("humanity") && play.hasComponent("position")){
                    var pos = play.getComponent("position");
                    if(gameHandler.map.checkMapID(pos.vector.x, pos.vector.y, pos.vector.z) == mapID){
                        return true
                    }
                }
            }
            return false;
        }
    },
    returnTrue : function(){
        return true;
    }
}

var triggerResults = {
    alarm : function(){
        alert("A TRIGGER HAS OCCURRED!");
    }
}
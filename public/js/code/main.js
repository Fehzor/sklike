// This is called an IFFY, it prevents variables from leaking outside
// of the file scope by enclosing them in a function that's run immediately.
// Javascript is weird.

loadResources(function(resources) {
    //Initializes everything.
    
    gameHandler.init();
    
    //The various AI things will normally be held by a gate and used repeatedly.
    var startState = AICreator.generate(1,enemyActionSets.eyeDemonSetA);
    var stateComponent = new enemyMachine(startState);
    var aiComponent = new enemyAI(aiFunctions.idleUntil);
    
    var model = new mesh("eyedemon","eyedemon",.285);
    

    var enemy = new entity([
       model,
       new position(model.model.position),
       new velocity(.07),
       new rotation(angleFunctions.faceTarget),
       new target(targetFunctions.selectPlayer,targetFunctions.dedicated),
       new mapComponent(gameHandler.map),
       new hitCylinder(.2,1,[[1,3]],(AFFECTS_PLAYERS | AFFECTS_ENEMIES)),
       new flag(AFFECTS_ENEMIES),
       new networked(true),
       stateComponent,
       aiComponent
    ]);
    
    var enemyPosition = enemy.getComponent("position").vector;
    var playerPosition = gameHandler.player.getComponent("position").vector;
    enemyPosition.x = playerPosition.x-3;
    enemyPosition.y = playerPosition.y-3;
    
    gameHandler.addEntity(enemy);
    
    function render(){
        gameHandler.renderer.render(gameHandler.scene, gameHandler.camera);
    };
    
    function mainLoop(timestamp){
        render();
        gameHandler.runSystems();
        requestAnimationFrame(mainLoop);
    }
    
    mainLoop(performance.now());
});

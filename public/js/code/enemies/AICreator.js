/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Generates a set of states for use with an enemy.
var AICreator = {
    //This is the main function for this thing. 
    generate : function(tier,set,base){
        if(tier == 1){
            //Get some functions.
            var startF = superRandom.randomElement(set.START);
            var traverseF = superRandom.randomElement(set.TRAVERSE);
            var dodgeF = superRandom.randomElement(set.DODGE);
            
            var startS = new enemyState(startF,TYPE_IDLE);
            var traverseS = new enemyState(traverseF, TYPE_TRAVERSE);
            var dodgeS = new enemyState(dodgeF,TYPE_DODGE);
            
            startS.options.push(traverseS);
            
            traverseS.options.push(dodgeS);
            traverseS.options.push(startS);
            
            dodgeS.options.push(traverseS);
            dodgeS.options.push(startS);
            
            return startS;
        } else {
            //Add on to the base state machine here via transversing it
        }
    }
}

var enemyActionSets = {
    //Set A is for T1 = pacifist enemies;
    eyeDemonSetA : {
        START : [
            stateFunctions.waitUntilClose(2.5),
            stateFunctions.waitUntilClose(7)
        ],
        TRAVERSE : [
            stateFunctions.moveTowardsTarget(2),
            stateFunctions.moveTowardsTarget(4)
        ],
        DODGE : [
            //This enemy circles the player sometimes.
            stateFunctions.strafeClockwise,
            stateFunctions.strafeCounterClockwise,
            //Rising and falling, as this enemy is airborn
            stateFunctions.aORb(
                    stateFunctions.descend(.3),
                    stateFunctions.rise(1.2))
        ]
    }
}
/*
 * 
    ],
    eyeDemonSetAMovement : [
        stateFunctions.moveTowardsTarget(.3),
        stateFunctions.moveTowardsTarget(3),
        stateFuctinos.strafeClockwise,
        stateFunctions.strafeCounterClockwise
    ]
 */
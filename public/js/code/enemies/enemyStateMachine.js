/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Constants for marking enemyStates with
const TYPE_IDLE = 0;
const TYPE_DODGE = 1;
const TYPE_TRAVERSE = 2;
const TYPE_ATTACK = 3;

//Build a network of these.
//options = a list of other states to tie to this state
//func = a function(entity) that determines what this state does.
//func returns true if it did something, false if it didn't really.
class enemyState {
    constructor(func,type){
        this.options = [];
        this.func = func;
        this.type = type | false;
    }
}

//Each enemy will have one of these
class enemyMachine extends component{
    constructor(start){
        super("state");
        
        //Keep track of where we are for this instance
        this.start = start;
        this.current = start;
        this.steps = 0; //How many steps has it been since an AI change?
        this.stateInfo = {};
    }
}

//Seperate from the machine, this actually decides what happens next.
//Takes in a function which acts on an enemyMachine component
class enemyAI extends component{
    constructor(func){
        super("ai");
        this.func = func;
    }
}

//Runs the things.
class aiSystem extends system{
    constructor(){
        super(["state","ai"]);
    }
        
    apply(entity){
        var machine = entity.getComponent("state");
        var ai = entity.getComponent("ai");
        
        var done = machine.current.func(entity);
        
        ai.func(machine, done);
        
        machine.steps+=1;
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var gameHandler = {
    entityList : [],
    map : new metaMap(),
    scene : new THREE.Scene(),
    renderer : new THREE.WebGLRenderer(),
    camera : new THREE.PerspectiveCamera(80,window.innerWidth / window.innerHeight, 1, 1000),
    player : false,
    systems : [
        new controlResetSystem(),
        new mouseControlsSystem(),
        new keyboardControlsSystem(),
        new controlMovementSystem(),
        new targetSystem(),
        new aiSystem(),
        new rotationSystem(),
        new heightColliderSystem(),
        new entityColliderSystem(),
        new movementSystem(),
        new cameraSystem(),
        new triggerSystem()
    ],
    peerSystem : null,
    loadMap(mapData){
        this.map = metaMap.decode(mapData);
        //console.log(this.map.constructor)
        this.clearScene();
        this.clearEntities();
        
        this.addEntity(this.player);
        this.player.getComponent("mapComponent").map = this.map;
        
        this.map.init(this.scene,this.player);
        
        this.addLighting();
    },
    addEntity(entity){
        this.entityList.push(entity);
        for(var i = 0; i < this.systems.length; ++i){
            this.systems[i].checkEntity(entity);
        }
        if(entity.hasComponent("model")){
            entity.getComponent("model").addToScene(this.scene);
        }
    },
    clearEntities(){
        gameHandler.entityList = [];
        for(var i = 0; i < this.systems.length; ++i){
            this.systems[i].entityList = [];
        }
    },
    updateEntity(entity){ //rechecks and entity against systems.
        for(var i = 0; i < this.systems.length; ++i){
            this.systems[i].checkEntity(entity);
        }
    },
    runSystems(){
        for(var i = 0; i < this.systems.length; ++i){
            this.systems[i].run();
        }
    },
    clearScene(){
        while(gameHandler.scene.children.length){
            gameHandler.scene.remove(gameHandler.scene.children[0])
        }
    },
    addLighting(){
        const lightA = new THREE.DirectionalLight(0xffffff, 0.7);
        lightA.position.x = -128;
        lightA.position.y = -256;
        lightA.position.z = 512;
        const lightB = new THREE.DirectionalLight(0xffffff, 0.3);
        lightB.position.x = -96;
        lightB.position.y = 128;
        lightB.position.z = 320;
        this.scene.add(lightA)
        this.scene.add(lightB)
    },
    init() {
        var mc = new mapComponent(gameHandler.map);
        var cam = new cameraData(this.camera);
        this.player = player.generatePlayer();
        this.player.pushComponent(mc);
        this.player.pushComponent(cam);
        
        this.addLighting();

        this.renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(this.renderer.domElement);

        this.addEntity(this.player);

        var ps = new peerSystem();
        

        this._onResize = this.onResize.bind(this);
        window.addEventListener('resize', this._onResize);
        this.map.init(this.scene, this.player);
    },
    deinit() {
        window.removeEventListener('resize', this._onResize);
    },
    onResize(e) {
        console.log('On resize')
        const w = window.innerWidth;
        const h = window.innerHeight;
        this.camera.aspect = w / h;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(w, h);
    }
};

function onGamepadConnected(e) {
    if(onGamepadConnected.initiated) return;
    onGamepadConnected.initiated = true;
    const kbSys = gameHandler
      .systems
      .find(sys => sys.constructor === keyboardControlsSystem);
    const index = gameHandler.systems.indexOf(kbSys);
    gameHandler.systems.splice(index + 1, 0, new gamepadControlsSystem());

    onGamepadConnected.handled[e.gamepad.id] = true;
    gameHandler.player.pushComponent(new gamepadControls(e.gamepad));
    console.log('Gamepad connected')
}
onGamepadConnected.handled = {};
onGamepadConnected.initiated = false;
window.addEventListener("gamepadconnected", onGamepadConnected)
if(navigator.getGamepads) {
  for(const pad of navigator.getGamepads()) {
    if(!pad) continue;
    onGamepadConnected({gamepad: pad});
  }
}

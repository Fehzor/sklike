
//Miscelaineous functions featured here!
var mapping = {
    constants : {
      floor : {
        // material : new THREE.MeshNormalMaterial(),
        material : new THREE.MeshLambertMaterial({color: 0xedc9af}), 
        geometry : new THREE.BoxGeometry( 1, 1, 1 ),
      },
      cube : {
        // material : new THREE.MeshBasicMaterial( {color : 0x0e8e3d} ),
        material : new THREE.MeshLambertMaterial( {color : 0x0e8e3d} ),
        geometry : new THREE.BoxGeometry( 1, 1, 1 ),
      },
    },
    
    //Will likely need to generate objects later?
    generateCube : function(){
        const geometry = mapping.constants.cube.geometry;
        const material = mapping.constants.cube.material;
        const cube = new THREE.Mesh( geometry, material );
        return cube;
    },

    generateFloor : function (density){
        const geometry = mapping.constants.floor.geometry;
        const material = mapping.constants.floor.material;
        const cube = new THREE.Mesh( geometry, material );
        cube.scale.z = density;
        return cube;
    },
    
    UNDIRECTIONAL : -1,
    NORTH : 0,
    EAST : 1,
    SOUTH : 2,
    WEST : 3
    
}

//The connection between maps, as an object.
class mapPortal{
    constructor(direction,location,owner){
        this.direction = direction;
        this.location = location;
        this.owner = owner;
        this.connected = false;
    }
    
    //For instance, an east portal can ONLY connect to a west portal.
    //Otherwise they'd overlap.
    compatable(otherPortal){
        var dirA = this.direction;
        var dirB = otherPortal.direction;
        
        if(dirA == mapping.NORTH && dirB == mapping.SOUTH ||
                dirB == mapping.NORTH && dirA == mapping.SOUTH){
            return true;
        }
        
        if(dirA == mapping.EAST && dirB == mapping.WEST ||
                dirB == mapping.EAST && dirA == mapping.WEST){
            return true;
        }
        
        if(dirA == mapping.UNDIRECTIONAL && dirB == mapping.UNDIRECTIONAL){
            return true;
        }
        
        return false;
    }
};

//The class that represents some portion of the map. A node, if you will.
class mapData {
    constructor(){
        this.mapID = -1;
        
        //Where the map's origin is in relation to 0,0,0
        this.mapStart = [0,0,0];
        
        //Where the player's starting position on this map is in relation 
        //to the map's origin
        this.playerStart = [1,1,0];
        
        //All of the doorways, as coordinates and on what face they are.
        //this.portals = [];
        
        //*
        //.5 makes it to where it connects properly.
        var offset = 0;
        
        this.portals = [new mapPortal(mapping.SOUTH,[4,6.5,offset],this),
            new mapPortal(mapping.SOUTH,[1,6.5,offset],this),
            new mapPortal(mapping.NORTH,[1,-.5,offset],this),
            new mapPortal(mapping.NORTH,[4,-.5,offset],this),
            new mapPortal(mapping.UNDIRECTIONAL,[3,3,1.8],this),
            new mapPortal(mapping.EAST,[6.5,3,offset],this),
            new mapPortal(mapping.WEST,[-.5,3,offset],this)];
        //*/
        
        //shuffle this to prevent very deterministic patterns while connecting...
        superRandom.shuffleArray(this.portals);
        
        //How big the map is.
        this.sizeX = 7; //"rows"
        this.sizeY = 7; //"cols"
        
        //The map itself, as a flat object.
        //0 = player can move = nothing
        //1 = blocked
        this.blockData = [
            0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,
        ];
    
        //How dense the ground on the map is.
        this.density = 3;
        //How tall the map is.
        this.height = 1.5;
    
        //A map of the base heights of each individual block. This should be
        //useful for stairs and details etc.
        //0 = base height,
        this.heightData = [
            0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,
            0,0,.125,.25,.125,0,0,
            0,0,.25,.5,.25,0,0,
            0,0,.125,.25,.125,0,0,
            0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,
        ];
    
        //Eventually this will contain data that spawns in things.
        //Probably like-
        //
        //[[instance#,params,etc.],[instance#,params,etc.]]
        //
        //So maybe you'd have a button that spawns beasts. It may trigger 
        //trigger # 3...
        //
        //Then the beasts would be marked as trigger # 3s? Something like that.
        //
        this.instances = [];
    }

    // Encode map to as few bytes as is practical,
    // optimized for being sent across the net
    encode() {
      // Each cell in blocks contains type + height of 2 tiles
      // compressed to 8 bit
      const blocks = Array(Math.ceil(this.sizeX * this.sizeY / 2));
      for(var i = 0; i < this.blockData.length; i += 2) {
          blocks[i / 2] =
              this.blockData[i] +
              (this.heightData[i] * 8 << 8) +
              ((this.blockData[i + 1] || 0) << 16) +
              ((this.heightData[i + 1] || 0) * 8 << 24);
      }
      return {
          id: this.mapID,
          w: this.sizeX,
          h: this.sizeY,
          s: this.mapStart,
          pl: this.playerStart,
          ps: this.portals.map(p => {
              return {
                  d: p.direction,
                  l: p.location,
              }
          }),
          bd: blocks,
      };
    }

    static decode(data) {
        const ret = new mapData;
        ret.mapID = data.id;
        ret.sizeX = data.w;
        ret.sizeY = data.h;
        ret.mapStart = data.s;
        ret.playerStart = data.pl;
        ret.portals = data.ps.map(p => new mapPortal(p.d, p.l, ret));
        ret.blockData.length = data.w * data.h;
        ret.heightData.length = ret.blockData.length;
        for(var i = 0; i < ret.blockData.length; ++i) {
            const cell = data.bd[(i / 2) | 0];
            var block, height;
            if(i % 2) {
              block = (cell >> 16) & 0xFF;
              height = ((cell >> 24) & 0xFF) / 8;
            } else {
              block = cell & 0xFF;
              height = ((cell >> 8) & 0xFF) / 8;
            }
            ret.blockData[i] = block;
            ret.heightData[i] = height;
        }
        return ret;
    }
    
    //Check what block is at global coordinates x,y.
    checkBlock(x, y){
        var x1 = Math.floor(x - this.mapStart[0]);
        var y1 = Math.floor(y - this.mapStart[1]);
        
        if(x1 < 0 || x1 >= this.sizeY || y1 < 0 || y1 >= this.sizeX){
            return -1;
        } else {
            return this.blockData[y1 * this.sizeX + x1]
        }
    }
    
    //Check what block is at global coordinates x,y.
    checkHeight(x, y){
        var x1 = Math.floor(x - this.mapStart[0]);
        var y1 = Math.floor(y - this.mapStart[1]);
        
        return this.heightData[y1 * this.sizeX + x1]
    }
    
    //Is the global point x,y,z contained in this map segment?
    contains(x,y,z){
        if(z > this.mapStart[2]-this.density && z < this.mapStart[2]+this.height){
          return (
              x > this.mapStart[0] &&
              x < (this.mapStart[0] + this.sizeX) &&
              y > this.mapStart[1] &&
              y < (this.mapStart[1] + this.sizeY)
          )
        }
        return false;
    }
    
    //Moves the player to this map's starting location.
    setPlayerStart(player){
        var pos = player.getComponent("position");
        pos.vector.x = this.playerStart[0]+this.mapStart[0];
        pos.vector.y = this.playerStart[1]+this.mapStart[1];
        pos.vector.z = this.playerStart[2]+this.mapStart[2];
    }
    
    
    //Adds the map to a scene.
    buildMap(scene){
        for (var i = 0; i < this.sizeY; i++) {
            for(var j = 0; j < this.sizeX; j++){
                const index = i * this.sizeX + j;
                if(this.blockData[index] == 1){
                    var cube = mapping.generateCube();
                    cube.position.x = (i)+this.mapStart[0] + .5;
                    cube.position.y = (j)+this.mapStart[1] + .5;
                    cube.position.z = this.mapStart[2];
                    scene.add( cube );
                }
                
                if(this.blockData[index] !== -1){
                    var floor = mapping.generateFloor(this.density);
                    floor.position.x = (i)+this.mapStart[0] + .5;
                    floor.position.y = (j)+this.mapStart[1] + .5;
                    floor.position.z = this.mapStart[2] - (this.density/2) - .5 + this.heightData[index];
                    scene.add( floor );
                }
            }
        }
    }
    
    //Connect to antoher map- the main method that is called.
    //Owner = the metamap responsible for connecting.
    connect(owner, otherMap){
        //First check our portals
        var port;
        var otherPortal;
        for(var i = 0; i < this.portals.length; ++i){
            port = this.portals[i];
            if(!port.connected){
                //When we find an acceptable portal for us we check their portals-
                for(var j = 0; j < otherMap.portals.length; ++j){
                    if(otherMap.portals[j].compatable(port)){
                        //Set the coordinates for the map based on 3 factors so far-
                        otherPortal = otherMap.portals[j];

                        //Make it overlap with the map it's being added to.
                        otherMap.mapStart[0] = port.owner.mapStart[0];
                        otherMap.mapStart[1] = port.owner.mapStart[1];
                        otherMap.mapStart[2] = port.owner.mapStart[2];

                        //Move the origin of the map to where the node is.
                        otherMap.mapStart[0]+=port.location[0];
                        otherMap.mapStart[1]+=port.location[1];
                        otherMap.mapStart[2]+=port.location[2];

                        //After this, the thing overlaps.
                        otherMap.mapStart[0]-=otherPortal.location[0];
                        otherMap.mapStart[1]-=otherPortal.location[1];
                        otherMap.mapStart[2]-=otherPortal.location[2];
                        
                        if(owner.checkOverlap(otherMap)){
                            //The portals are compatable, so we link the maps.
                            port.connected = true;
                            otherPortal.connected = true;
                            
                            

                            //It worked, so we return true.
                            return true;
                        } else {
                            otherMap.mapStart = [0,0,0];
                        }
                    }
                }
            }
        }
        return false;
    }
};

//This class is intended to handle mapdatas
//It is effectively a complete map, ready to be ran.
class metaMap {
    //Map construction stuff here?
    //Eventually it would presumably load maps from files etc.
    constructor(){
        var mapID = 1;
        this.start = new mapData();
        this.start.mapID = mapID;
        mapID += 1;
        this.maps = [this.start];
        
        for(var i = 0; i < 33; ++i){
            var temp = new mapData();
            var index = Math.floor(Math.random() * this.maps.length);
            var addTo = this.maps[index];
            
            if(addTo.connect(this,temp)){
                temp.mapID = mapID;
                mapID += 1;
                this.maps.push(temp);
            }
        }
    }

    encode() {
        return {
          s: this.maps.indexOf(this.start),
          ms: this.maps.map(map => map.encode())
        }
    }

    static decode(data) {
        const ret = new metaMap;
        ret.maps = data.ms.map(mapData.decode);
        ret.start = ret.maps[data.s];
        return ret;
    }
    
    //Initialize the map to actually be ran.
    init(scene, play){
        for(var i = 0; i < this.maps.length; ++i){
            this.maps[i].buildMap(scene);
        }
        this.start.setPlayerStart(play);
        var model = play.getComponent("model");
        scene.add(model.model);
    }
    
    checkMapID(x,y,z){
        //All this does is search for the correct map to use.
        for(var i = 0; i < this.maps.length; ++i){
            if(this.maps[i].contains(x,y,z)){
                //console.log("MAP ID- "+this.maps[i].mapID);
                return this.maps[i].mapID;
            }
        }
        //console.log("MAP ID- "+"-1");
        return -1;
    }
    
    //This checks what block is where across all maps.
    checkBlock(x, y, z){
        //All this does is search for the correct map to use.
        for(var i = 0; i < this.maps.length; ++i){
            if(this.maps[i].contains(x,y,z)){
                var get = this.maps[i].checkBlock(x,y);
                if(get !== -1){
                    //Whenever there's a hit, we move it to the front
                    //This way next time it gets hit again, because same map,
                    //It's at the forefront.
                    arrayUtils.arrayMove(this.maps,i,0);
                    return get;
                }
            }
        }
        
        return -1;
    }
    
    //Checks the height
    //0 = base height; -1 = out of bounds; everythine else = some height
    checkHeight(x,y,z){
        var lowestPoint = Infinity;
        var lowestDistance = Infinity;
        
        for(var i = 0; i < this.maps.length; ++i){
            if(this.maps[i].contains(x,y,z)){
                var get = this.maps[i].checkBlock(x,y);
                switch(get){
                    case -1:
                        break;
                    case 0:
                        var baseheight = this.maps[i].mapStart[2] + this.maps[i].checkHeight(x,y);
                    
                        if(z - baseheight < lowestDistance){
                            lowestPoint = baseheight;
                            lowestDistance = z - baseheight;
                        }
                        break;
                    case 1:
                        var baseheight = this.maps[i].mapStart[2] + this.maps[i].checkHeight(x,y);
                        
                        if(z - baseheight+1 < lowestDistance){
                            lowestPoint = baseheight+1;
                            lowestDistance = z - (baseheight + 1);
                        }
                        break;
                }
            }
        }
        
        return lowestPoint; 
    }
    
    //Checks if a map, toAdd, is acceptable to add to the meta.
    checkOverlap(toAdd){
        for(var i = 0; i < toAdd.sizeX;  ++i){
            for(var j = 0; j < toAdd.sizeY; ++j){
                var x1 = i + toAdd.mapStart[0]+.5;
                var y1 = j + toAdd.mapStart[1]+.5;
                var z1 = toAdd.mapStart[2] + .5;
                
                if(this.checkBlock(x1,y1,z1) !== -1){
                    return false;
                }
            }
        }
        return true;
    }
};

 //Connect up the example map- this will need redone at some point as factories.
        /*
        for(var i = 0; i < 33; ++i){
            
            
            var temp = new mapData();
            
            
            if( i%4 == 2 ){
                temp.sizeX = 3;
                temp.sizeY = 3;
                
                temp.blockData = [
                    [0,0,0],
                    [0,1,0],
                    [0,0,0]
                ];
                
                temp.heightData = [
                    [0,0,0],
                    [0,0,0],
                    [0,0,0]
                ];
                
                temp.portals =  [
                    new mapPortal(mapping.UNDIRECTIONAL,[1,1,.5],temp)
                ];
                
                temp.density = .3;
                
            }
            
            
            var index = Math.floor(Math.random() * this.maps.length);
            var addTo = this.maps[index];
            
            if(addTo.connect(this,temp)){
                this.maps.push(temp);
            }
        }
        */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var boolets = {
    //For the first gun to fire
    booletA : {
        shared : [
             new hitCylinder(.2,1,[[1,3.3]],(AFFECTS_ENEMIES)),
             new flag(AFFECTS_PLAYER_BULLETS),
        ],
        velocity : .1,
        distance : 8
    }
}

class booletGenerator{
    constructor(dataSet){
        this.data = dataSet;
    }
    
    generate(){
        var components = [];
        for( var comp in this.data.shared ){
            components.push(comp);
        }
        components.push(new velocity(this.data.velocity));
        components.push(new sphere(.2));
        components.push(new position(new THREE.Vector3(0,0,0)));
        components.push(new networked(true));
    }
}
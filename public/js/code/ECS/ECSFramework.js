/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



ECSFramework = {
    //The ID creation function/variable.
    nextID : 0,
    generateID : function(){
        this.nextID += 1;
        return this.nextID;
    }
}

//Consists of an ID and components
class entity{
    //Entities require components
    constructor(components){
        this.ID = ECSFramework.generateID()*100;
        this.components = new Map();
        for(var i = 0; i < components.length; ++i){
            this.components.set(components[i].name,components[i]);
        }
        this.alive = true;
    }
    
    getComponent(name){
        return this.components.get(name);
    }
    
    hasComponent(name){
        return this.components.has(name);
    }
    
    //Generates a readable dataPacket of this object.
    generateDataPacket(){
        var ret = {ID : this.ID};
        for (var [key, value] of this.components) {
            var set = this.components.get(key).toData();
            if(this.components.get(key).networkable){
                ret[key] = set;
            }
        }
        return ret;
    }
    
    pushComponent(component){
        this.components.set(component.name,component);
        gameHandler.updateEntity(this);
    }
}

//Is given to entities
class component{
    //Each component has a name.
    constructor(name,networkable){
        this.name = name;
        this.networkable = networkable;
    }
    
    toData(){return false;}
    applyData(data){};
    
}

//Is applied to entities based on components
class system{
    //Fields = what sort of components this system requires.
    constructor(fields){
        this.fields = fields;
        this.entityList = [];
    }
    
    checkEntity(entity){
        if(this.entityList.includes(entity)){
            for(var i = 0; i < this.fields.length; ++i){
                if(!entity.components.has(this.fields[i])){
                    if(this.entityList.includes(entity)){
                        this.entityList.splice(this.entityList.indexOf(entity),1);
                    }
                    return false;
                }
            }
        } else {
            for(var i = 0; i < this.fields.length; ++i){
                if(!entity.components.has(this.fields[i])){
                    return false;
                }
            }
            this.entityList.push(entity);
            return true;
        }
    }
    
    run(){
        for(var i = 0; i < this.entityList.length; ++i){
            this.apply(this.entityList[i]);
        }
    }
}


class factory {
  constructor(generator) {
    this.instances = [];
    this.count = 0;
    this.generator = generator;
  }

  create() {
    if(this.count === this.instances.length) this.instances.push(generator());
    return this.instances[this.count++];
  }

  release(instance) {
    --this.count;
    const index = this.instances.indexOf(instance);
    this.instances[index] = this.instances[this.count];
    this.instances[this.count] = instance;
  }
}
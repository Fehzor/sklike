/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



//Moves the cameraData to the position.
class cameraSystem extends system{
    constructor(){
        super(["position", "cameraData"]);
    }
    
    apply(entity){
        //Move the camera into position
        var cam = entity.getComponent("cameraData");
        var pos = entity.getComponent("position");
        
        cam.camera.position.x = pos.vector.x + cam.offsets.x;
        cam.camera.position.y = pos.vector.y + cam.offsets.y;
        cam.camera.position.z = pos.vector.z + cam.offsets.z;
    }
}

//Checks height differences etc.
class heightColliderSystem extends system{
    constructor(){
        super(["position","velocity","mapComponent"]);
        this.ledgeLimit = .35;
    }
    
    apply(entity){
        var pos = entity.getComponent("position");
        var vel = entity.getComponent("velocity");
        var mapC = entity.getComponent("mapComponent");
        
        var x1 = pos.vector.x + vel.vector.x;
        var y1 = pos.vector.y + vel.vector.y;
        var z1 = pos.vector.z + vel.vector.z;
        
        var height = mapC.map.checkHeight(x1,y1,z1);
        var heightDiff = Math.abs(height - pos.vector.z);
        
        //Is there an issue with going in the direction?
        if(heightDiff > this.ledgeLimit){
            //Yes. Was that issue with going in the x direction?
            height = mapC.map.checkHeight(x1,pos.vector.y,z1);
            heightDiff = Math.abs(height - pos.vector.z);
            
            if(heightDiff > this.ledgeLimit){
                //There WAS an issue with X. Is there an issue with Y?
                height = mapC.map.checkHeight(pos.vector.x,y1,z1);
                heightDiff = Math.abs(height - pos.vector.z);
                if(heightDiff > this.ledgeLimit){
                    vel.vector.y = 0;
                    vel.vector.x = 0;
                } else {
                    vel.vector.x = 0;
                    pos.vector.z = height;
                }
            } else{
                //It wasn't an issue with x, so it must have been y; reset that.
                vel.vector.y = 0;
                pos.vector.z = height;
            }
        } else {
            //Lock the entity to the z height for now.
            pos.vector.z = height;
        }
    }
}

//Checks collisions between entities.
class entityColliderSystem extends system{
    constructor(){
        super(["position","collider","flag"]);
    }
    
    run(){
        for(var i = 0; i < this.entityList.length - 1; ++i){
            for(var j = i+1; j < this.entityList.length; ++j){
                var a = this.entityList[i];
                var b = this.entityList[j];
                
                //Check the flags here.
                var aMask = a.getComponent("collider").mask;
                var bMask = b.getComponent("collider").mask;
                
                var aFlag = a.getComponent("flag").mask;
                var bFlag = b.getComponent("flag").mask;
                
                var AB = aMask & bFlag;
                var BA = bMask & aFlag;
                
                if(AB || BA){//If either collides with the other, proceed.
                    var posA = a.getComponent("position").vector;
                    var posB = b.getComponent("position").vector;

                    var colliderA = a.getComponent("collider");
                    var colliderB = b.getComponent("collider");

                    //CODE FOR CYLINDER COLLISIONS

                    //See if they're close enough
                    var temp = posA.z;
                    posA.z = posB.z;
                    var dist = posA.distanceTo(posB);
                    posA.z = temp;

                    var check = colliderA.radius + colliderB.radius;

                    if(dist > check) continue;

                    //Now check height
                    if(posA.z + colliderA.height < posB.z) continue;
                    if(posB.z + colliderB.height < posA.z) continue;
                    this.apply(a,b,AB,BA);
                }
            }
        }
    }
    
    apply(a,b,AB,BA){
        var aFuncs = a.getComponent("collider").effects;
        var bFuncs = b.getComponent("collider").effects;
        
        //If AB or BA is false, it's a miss.
        if(BA){
            for(var i = 0; i < aFuncs.length; ++i){
                aFuncs[i](b,a);
            }
        }
        
        if(AB){
            for(var i = 0; i < bFuncs.length; ++i){
                bFuncs[i](a,b);
            }
        }
    }
}

class movementSystem extends system{
    constructor(){
        super(["position", "velocity"]);
    }

    apply(entity){
       var pos = entity.getComponent("position");
       var vel = entity.getComponent("velocity");

       pos.vector.add(vel.vector);
       
       if(entity.hasComponent("model")){
           var model = entity.getComponent("model").model;
           var to = pos.vector
           model.position.set(to.x,to.y,to.z);
       }
    }
}

//Faces the model in the direction the model is traveling.
class rotationSystem extends system {
    constructor() {
        super(["rotation","model"]);
    }

    apply(entity){
        var fun = entity.getComponent("rotation").func;
        fun(entity);
    }
}

//How the enemies choose their target.
class targetSystem extends system {
    constructor() {
        super(["target"])
    }
    
    apply(entity){
        var target = entity.getComponent("target");
        if(target.target == false || target.done()){
            //console.log("TARGET: "+target.select(entity));
            target.target = target.select(entity);
        }
    }
}

// Resets general controls object
// Run before specific control systems or after any logic that checks controls
class controlResetSystem extends system{
    constructor(){
        super(["controls"]);
    }

    apply(entity){
        var ctrl = entity.getComponent("controls");
        
        ctrl.x = 0;
        ctrl.y = 0;
        ctrl.rx = 0;
        ctrl.ry = 0;
        ctrl.pressed = 0;
        ctrl.released = 0;
    }
}

// Assigns keyboard controls to general controls and resets keyboard controls
// Run between general control system and logic that needs to check controls
class keyboardControlsSystem extends system{
    constructor(){
        super(["controls", "keyboardControls"]);
    }

    apply(entity){
        const ctrl = entity.getComponent("controls");
        const kb = entity.getComponent("keyboardControls");

        ctrl.x += +kb.right - +kb.left || 0;
        ctrl.y += +kb.up - +kb.down || 0;
        ctrl.held = (ctrl.held | kb.pressed) & ~kb.released;
        ctrl.pressed |= kb.pressed;
        ctrl.released |= kb.released;

        kb.pressed = 0;
        kb.released = 0;
    }
}

class mouseControlsSystem extends system{
    constructor(){
        super(["controls", "mouseControls"]);
    }
    
    apply(entity){
        const mouse = entity.getComponent("mouseControls"); 
        if(!mouse.isMoved) return;
        const ctrl = entity.getComponent("controls");

        ctrl.rx = mouse.x / mouse.width - 0.5;
        ctrl.ry = mouse.y / mouse.height - 0.5;

        mouse.isMoved = false;
    }
}

class gamepadControlsSystem extends system{
    constructor(){
        super(["controls", "gamepadControls"]);
    }

    run(){
        const pads = navigator.getGamepads();
        for(var i = 0; i < this.entityList.length; ++i){
            // Update gamepad state
            const entity = this.entityList[i];
            const padComp = entity.getComponent("gamepadControls");
            if(!padComp.enabled) continue;
            const ctrl = entity.getComponent("controls");
            const pad = pads[padComp.padIndex];

            padComp.pressed = 0;
            padComp.released = 0;
            for(var i = 0; i < pad.buttons.length; ++i) {
                // TODO: Button mapping
                const mask = 1 << i;
                const pressed = pad.buttons[i].pressed;
                if(pressed && !(padComp.held & mask)) {
                    padComp.held |= mask;
                    padComp.pressed |= mask;
                } else if(!pressed && (padComp.held & mask)) {
                    padComp.held &= ~mask;
                    padComp.released |= mask;
                }
            }

            // Copy gamepad state
            const x = pad.axes[0] || 0;
            const y = pad.axes[1] || 0;
            if(x < -padComp.deadzone || x > padComp.deadzone) ctrl.x += x;
            if(y < -padComp.deadzone || y > padComp.deadzone) ctrl.y -= y;
            const rx = pad.axes[2] || 0;
            const ry = pad.axes[3] || 0;
            if(rx < -padComp.deadzone || rx > padComp.deadzone) ctrl.rx += rx;
            if(ry < -padComp.deadzone || ry > padComp.deadzone) ctrl.ry -= ry;
            ctrl.held = (ctrl.held | padComp.pressed) & ~padComp.released;
            ctrl.pressed |= padComp.pressed;
            ctrl.released |= padComp.released;
        }
    }
}

// Applies controls to movement
class controlMovementSystem extends system{
    constructor(){
        super(["controls", "velocity"]);
    }

    apply(entity){
        var ctrl = entity.getComponent("controls");
        var velocity = entity.getComponent("velocity");
        
        velocity.vector.x = ctrl.x;
        velocity.vector.y = ctrl.y;
        if(!ctrl.x && !ctrl.y) return;
        velocity.vector.normalize().multiplyScalar(velocity.max);
    }
}

class damageSystem extends system{
    constructor(){
        super(["health"]);
    }
}

//Checks triggers
class triggerSystem extends system{
    constructor(){
        super(["trigger"]);
    }
        
    apply(entity){
        var trig = entity.getComponent("trigger");
        //console.log(trig);
        if(trig.ready == true){
            
            if(trig.trigger()){
                trig.ready = false;
                trig.func();
            }
        }
    }
}

//Handles all network code.
class peerSystem extends system{
    constructor(){
        super(["networked"]);
        
        this.ID = -1; // 1 = host, 2 = second connection, 3 = third, 4 = etc.
        this.nextID = 2;// The next ID to assign.
        
        this.ghosts = new Map();
        
        this.connections = [];
        this.peer = new Peer({key: 'lwjd5qra8257b9'});
        
        this.peer.on('open',(id) => { 
            // alert(id); //UNCOMMENT THIS TO SEE THE ID ON START UP.
            document.getElementById('peer-stuff').classList.remove('hidden');
            const idInput = document.getElementById('peer-id')
            idInput.value = id;
            // Copy key on click
            idInput.addEventListener('click', function() {
              this.select();
              document.execCommand('copy');
            });
            document
              .getElementById('join-peer-button')
              .addEventListener('click', function() {
                  const id = document.getElementById('join-peer-id').value;
                  if(!id) {
                    messages.log('No ID specified.');
                  } else if(!gameHandler.peerSystem) {
                    messages.log('Networking not available.');
                  } else {
                    gameHandler.peerSystem.connect(id);
                  }
              })
            gameHandler.peerSystem = this;
            gameHandler.systems.push(gameHandler.peerSystem);
            
            for(var i = 0; i < gameHandler.entityList.length; ++i){
                gameHandler.peerSystem.checkEntity(gameHandler.entityList[i]);
            }
        });
        
        this.peer.on('connection',function(conn){
            messages.log(conn.peer + ' is connecting...');
            conn.on('open',() => {
                //alert("HOST SIDE CONNECTED");
                messages.log('Client joined');
                gameHandler.peerSystem.ID = 1;
                
                var starterPacket = {};
                starterPacket.LOADMAP = gameHandler.map.encode();
                starterPacket.SETID = gameHandler.peerSystem.connections.length + 2;
                conn.send(starterPacket);
                
                gameHandler.peerSystem.connections.push(conn);
            });
            conn.on('data',(data) => {
                //console.log('data received by host', data);
                gameHandler.peerSystem.receive(data);
            });
            conn.on('error',(error) => {
                //alert(error)
                messages.log(error);
            });
        });
        
    }
    connect(id){
        var conn = this.peer.connect(id);
        
        //For some reason this line breaks it- message not defined????
        messages.log('Connecting to ${id}...');
        
        conn.on('open',() => {
            //alert("CLIENT SIDE CONNECTED");
            messages.log('Connected.');
            //gameHandler.clearEntities();
            //conn.send('something');
        });
        conn.on('data', (data) => {
            gameHandler.peerSystem.receive(data);
        });
        conn.on('error', (error) => {
            messages.log(error);
            //alert(error);
        });
        
        this.connections.push(conn);
    }
    
    apply(entity){
        // console.log("Size- "+this.entityList.length);
        var networked = entity.getComponent("networked");
        if(networked.real){
            var packet = entity.generateDataPacket();//Generate a packet.
            packet.ID+=this.ID;
            //console.log("SENDING"+packet);
            this.relay(packet);
        } else {
            //It's a ghost so... we could just not register ghosts? Not sure.
        }
    }

    
    //Message all connections at once, excluding not if not is given.
    relay(message,not){
        if(this.ID == -1){
            return; //It isn't fully connected yet!
        }
        if(not){
            for(var i = 0; i < this.connections.length; ++i){
                //console.log("SENDING ("+i+")-" + message);
                if(not-2 !== i){
                    this.connections[i].send(message);
                }
            }
        } else {
            for(var i = 0; i < this.connections.length; ++i){
                this.connections[i].send(message);
            }
        }
    }
    
    receive(packet){
        if(packet.SETID){
            //alert("SETTING ID TO " + packet.SETID);
            this.ID = packet.SETID;
        }
        if(packet.LOADMAP){
            gameHandler.loadMap(packet.LOADMAP);
        }
        if(packet.COLLISION){
            
        }
        if(this.ghosts.has(packet.ID)){
            var ghost = this.ghosts.get(packet.ID);
            
            for(var propt in packet){
                var get = ghost.getComponent(propt+"");
                if(get && get.networkable){
                    get.applyData(packet[propt]);
                }
            }
        } else {
            this.createGhost(packet);
        }
        
        //If this is the host relay messages to the clients.
        if(this.ID == 1){
            var not = packet.ID % 100;
            this.relay(packet,not);
        }
    }
    
    createGhost(dataPacket){
        console.log(dataPacket);
        var ghost = new entity([]);
        for(var propt in dataPacket){
            var add = componentsUtil.reference(propt+"",ghost,dataPacket);
            if(add){
                ghost.components.set(add.name,add);
            }
        }
        
        this.ghosts.set(dataPacket.ID,ghost);
        gameHandler.addEntity(ghost);
    }
}

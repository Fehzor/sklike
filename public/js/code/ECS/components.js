/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

componentsUtil = {
    reference : function(string, entity, pack) {
        var propt = pack[string];
        var ret = false;
        switch(string){
            case "position":
                ret = new position(new THREE.Vector3(0,0,0));
                ret.applyData(propt);
                break;
            case "velocity":
                ret = new velocity(0,new THREE.Vector3(0,0,0));
                ret.applyData(propt);
                break;
            case "rotation":
                ret = new rotation(angleFunctions.applyRotation);
                ret.applyData(propt);
                break;
            case "model":
                if(propt){
                    ret = new mesh(propt[0],propt[1],propt[2]);
                } else {
                    ret = new cylinder();
                }
                break;
            case "flag":
                ret = new flag(propt[0]);
                break;
            case "collider":
                ret = new hitCylinder(propt[0],propt[1],propt[2],propt[3]);
                break;
        }
        return ret;
    }
}


//Designates that the camera will follow this entity.
class cameraData extends component{
    constructor(camera){
        super("cameraData");
        this.offsets = new THREE.Vector3(0, -6, 7);
        this.camera = camera;
        this.camera.rotation.x = Math.PI / 5;
    }
}

//Represents a fixed position in space.
class position extends component{
    constructor(vector){
        super("position",true);
        this.vector = vector;
    }
    
    toData(){
        return this.vector.toArray();
    }
    applyData(data){
        this.vector.x = data[0];
        this.vector.y = data[1];
        this.vector.z = data[2];
    }
}

//Represents a direciton of movement and the magnitude
class velocity extends component{
    constructor(max, vector){
        super("velocity",true); //WILL BE NETWORKABLE LATER
        this.max = max;
        this.vector = vector || new THREE.Vector3();
    }
    toData(){
        var ret = this.vector.toArray();
        ret.push(this.max);
        return ret;
    }
    applyData(data){
        this.vector.x = data[0];
        this.vector.y = data[1];
        this.vector.z = data[2];
        this.max = data[3];
    }
}

//angle is a function that returns the angle this object is facing.
class rotation extends component{
    constructor(angleFun){
        super("rotation",true);
        this.func = angleFun;
        this.angle = [0,0,0];
    }
    
    toData(){
        return this.angle;
    }
    
    applyData(data){
        this.angle = data;
    }
}

//Assigns a target
class target extends component{
    constructor(selectTarget,unTarget){
        super("target");
        this.select = selectTarget;
        this.done = unTarget;
        this.target = false;
    }
}

class controls extends component{
    constructor() {
        super("controls");
        this.x = 0;
        this.y = 0;
        this.rx = 0;
        this.ry = 0;
        this.held = 0;
        this.pressed = 0;
        this.released = 0;
    }
}

class mouseControls extends component{
    constructor(){
      super("mouseControls");
      this.x = 0;
      this.y = 0;
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.isMoved = false;

      window.addEventListener('resize', e => {
          this.width = window.innerWidth;
          this.height = window.innerHeight;
      })

      window.addEventListener('mousemove', e => {
          this.x = e.clientX;
          this.y = e.clientY;
          this.isMoved = true;
      })

      window.addEventListener('touchmove', e => {
          e.preventDefault()
          const touch = e.changedTouches[0]
          this.x = touch.clientX;
          this.y = touch.clientY;
          this.isMoved = true;
      }, true, {passive: false})
    }
}

class keyboardControls extends component{
    constructor(bindings){
        super("keyboardControls");
        this.left = false;
        this.right = false;
        this.up = false;
        this.down = false;
        this.held = 0;
        this.pressed = 0;
        this.released = 0;

        window.addEventListener('keydown', e => {
            var code = e.which || e.keyCode;
            var binding = bindings[e.which || e.keyCode];
            if(!binding) return;
            if('number' === typeof binding) {
                var mask = 1 << (binding - 1);
                this.pressed |= mask;
                this.held |= mask;
            } else {
                this[binding] = true;
            }
        });
        window.addEventListener('keyup', e => {
            var code = e.which || e.keyCode;
            var binding = bindings[e.which || e.keyCode];
            if(!binding) return;
            if('number' === typeof binding) {
                var mask = ~(1 << (binding - 1));
                this.pressed &= mask;
                this.held &= mask;
            } else {
                this[binding] = false;
            }
        });
    }
}

class gamepadControls extends component{
  constructor(pad) {
      super("gamepadControls");
      // In Chrome, pad is immutable. To get changed states,
      // the state must be polled with navigator.getGamepads()
      // For this reason, we store the pad's index rather than the pad itself.
      this.enabled = true;
      this.padId = pad.id;
      this.padIndex = pad.index;
      this.held = 0;
      this.pressed = 0;
      this.released = 0;
      this.deadzone = 0.1;

      window.addEventListener("gamepadconnected", e => {
          if(e.gamepad.id !== this.padId) return
          this.padIndex = e.gamepad.index;
          this.enabled = true;
      })
      window.addEventListener("gamepaddisconnected", e => {
          if(e.gamepad.id !== this.padId) return
          this.enabled = false;
      })
  }
}

//Creates a cylinder model for the player to use as a component.
class cylinder extends component{
    constructor(){
        super("model",true);
        var geometry =  new THREE.CylinderGeometry(.2, .2, 1, 10);
        // var material = new THREE.MeshNormalMaterial();
        var material = new THREE.MeshLambertMaterial({color: 0x00aaee});
        this.model = new THREE.Mesh(geometry, material);
        this.model.rotation.x = Math.PI / 2;
    }
    
    addToScene(scene){
        scene.add(this.model);
    }
}

//Creates a cylinder model for the player to use as a component.
class redCylinder extends component{
    constructor(){
        super("model",true);
        var geometry =  new THREE.CylinderGeometry(.2, .2, 1, 10);
        var material = new THREE.MeshBasicMaterial( {color : 0xFF0000} );
        this.model = new THREE.Mesh(geometry, material);
        this.model.rotation.x = Math.PI / 2;
    }
    
    addToScene(scene){
        scene.add(this.model);
    }
}

//Takes in a model or two as well as a function for resizing etc.
class mesh extends component{
    constructor(meshName, texName, scale){
        super("model", true);
        //Store these for later.
        this.meshName = meshName;
        this.texName = texName;
        
        var mesh = resources.models[meshName];
        var material = false;
        if(resources.textures[texName]){
            var tex = resources.textures[texName];
            material = new THREE.MeshLambertMaterial({map: tex});
        } else {
            material = resources.colors[texName];
        }
        
        
        var geometry = mesh;
        
        this.model = new THREE.Mesh(geometry, material);
        
        this.scale = scale;
        
        this.model.scale.multiplyScalar(scale);;
        //this.model.rotation.x = xRot;
        //this.model.up.z = zUp;
        
        this.model.up.z = 1;
        this.model.up.y = 0;
        this.model.up.x = 0;
    }

    addToScene(scene) {
        scene.add(this.model);
    }
    
    toData(){
        return [this.meshName, this.texName,this.scale];
    }
}

class sphere extends component{
    constructor(rad){
        super("model",true);
        var geometry =  new THREE.SphereGeometry(rad,11,11);
        var material = new THREE.MeshNormalMaterial();
        this.model = new THREE.Mesh(geometry, material);
    }
    
    addToScene(scene){
        scene.add(this.model);
    }
}

//Represents that an entity is in fact on a map.
class mapComponent extends component{
    constructor(map){
        super("mapComponent",false);
        this.map = map;
    }
}

const AFFECTS_PLAYERS = 0b0001;
const AFFECTS_ENEMIES = 0b0010;
const AFFECTS_PLAYER_BULLETS = 0b0100;
const AFFECTS_ENEMY_BULLETS = 0b1000;
const AFFECTS_ALL = 0b1111;
const AFFECTS_NONE = 0b0000;

//Effects is an array of functions(a,b) that are applied from a to b.
class hitCylinder extends component{
    constructor(radius,height,effects,mask){
        super("collider",true);
        this.radius = radius;
        this.height = height;
        this.packet = effects;
        this.effects = [];
        for(var i = 0; i < this.packet.length; ++i){
            var data = this.packet[i];
            var add = collisionEffects.getEffect(data[0],data[1]);
            this.effects.push(add);
        }
        this.mask = mask;
        
    }
    
    toData(){
        return [this.radius,this.height,this.packet,this.mask];
    }
}

//These are used as flags to mark that an object is part of a group.
//See above for AFFECTS that can fill in mask.
class flag extends component{
    constructor(mask){
        super("flag",true);
        this.mask = mask;
    }
    
    toData(){
        return [this.mask];
    }
}

//Grants an entity health
class health extends component{
    constructor(ticks){
        super("health");
        this.total = ticks * 100;
        this.remaining = this.total;
    }
}

//Takes in a function that is applied to health.
//defenseFormula(damage) returns a new damage amount.
class defense extends component{
    constructor(defenseFunction){
        super("defense");
        this.formula = defenseFunction;
    }
}

//Trigger = function that tells if triggered
//Func = function that behaves
//if(trigger())func();
class trigger extends component{
    constructor(trig,func){
        super("trigger",false);
        this.ready = true;
        this.trigger = trig;
        this.func = func;
    }
}

//Responsible for spawning in enemies at this entity's location
class spawner extends component{
    //Which map segment is it on by ID?
    //And which spawner type is it?
    constructor(mapID,typeID){
        this.mapID = mapID;
        this.typeID = typeID;
    }
}

//Networking classes below
//Responsible for network IDs.
var networkID = {
    ID : 0,
    getID : function(){
        this.ID = this.ID + 1;
        return this.ID;
    }
}
//Means this entity has a ghost component or is a ghost itself.
class networked extends component{
    constructor(real){
        super("networked", false); //THIS IS CLIENT SIDE ONLY
        this.real = real;
        this.ID = -1;
        if(this.real){
            this.ID = networkID.getID();
        }
    }
}

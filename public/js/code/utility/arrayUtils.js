/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var arrayUtils = {
    
    //Move an elementl from fromIndex to toIndex.
    arrayMove : function (arr, fromIndex, toIndex) {
        if(fromIndex == toIndex){
            return;
        }
        var element = arr[fromIndex];
        arr.splice(fromIndex, 1);
        arr.splice(toIndex, 0, element);
    }
};

//For all random generation functions...
var superRandom = {
    //Shuffles an array.
    shuffleArray : function (array){
        var currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    },
    
    //Returns true or false with equal probability.
    coinFlip(){
        var get = Math.random() * 10000;
        return get > 5000;
    },
    
    //Selects a random element in the list and returns it.
    randomElement(list){
        if(list.length > 0){
            var randomIndex = Math.floor(Math.random() * (list.length));
            console.log(randomIndex);
            return list[randomIndex];
        }
            return false;
    }
}

document.addEventListener('DOMContentLoaded', function() {
    const messages = document.getElementById('messages');
    const maxMessages = 10;
    var messageCount = 0;
    window.messages = {
        log(str) {
            var line
            if(messageCount >= maxMessages) {
              line = document.querySelector('.message.system');
            } else {
              line = document.createElement('div');
              line.classList.add('message', 'system');
            }
            messageCount++;
            line.textContent = str;
            messages.appendChild(line);
        },
    };

    function testMessage() {
        window.messages.log('Test message #' + messageCount);
        if(messageCount < 16) {
            setTimeout(testMessage, Math.random() * 10000 + 500);
        } else {
            window.messages.log('No more messages.')
        }
    }

    window.messages.log('Message system initiated');
    testMessage();
});

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var player = {
    generatePlayer : function (){
        
        //var playerModel = new mesh("catto","catto",.285);
        var playerModel = new mesh("catto","catto",.285);
        
        var play = new entity([
            playerModel,
            new position(playerModel.model.position),
            new velocity(0.12),
            new controls(),
            new mouseControls(),
            new keyboardControls({
              ['A'.charCodeAt()]: 'left',
              ['D'.charCodeAt()]: 'right',
              ['W'.charCodeAt()]: 'up',
              ['S'.charCodeAt()]: 'down',
              ['J'.charCodeAt()]: 1,
              ['I'.charCodeAt()]: 2,
            }),
            new hitCylinder(.2,1,[[0,false]],AFFECTS_ENEMIES),
            new networked(true),
            new flag(AFFECTS_PLAYERS),
            new rotation(angleFunctions.faceControls),
        ]);
        
        return play;
    }
};

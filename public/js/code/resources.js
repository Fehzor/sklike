var resources = {
    models: {},
    textures: {},
    colors: {}
};

function loadResources(cb) {
    const loaders = {
        models: new THREE.OBJLoader(),
        textures: new THREE.TextureLoader(),
    };
    var count = 0;

    function load(type, path) {
        ++count;
        loaders[type].load(path, function(object) {
            const name = path.split('/').pop().split('.')[0];
            if(type === 'models') object = object.children[0].geometry;
            resources[type][name] = object;
            if(!--count) {
              document.getElementById("loading").classList.add('hidden');
              cb(resources)
            }
        })
    };
    
    function loadColor(name,col){
        var mat = new THREE.MeshLambertMaterial( {color : col} );
        resources["colors"][name] = mat;
    };

    load('models', 'resources/catto.obj');
    load('textures', 'resources/catto.png');
    
    load('models', 'resources/eyedemon.obj');
    load('textures', 'resources/eyedemon.png');
    
    load('models', 'resources/cruster.obj');
    load('textures', 'resources/cruster.png');
    
    load('models', 'resources/invade.obj');
    
    loadColor("hotPink",0xFF6EB4);
    loadColor("brightRed",0xFF1111);
    loadColor("black",0x000000);
}
